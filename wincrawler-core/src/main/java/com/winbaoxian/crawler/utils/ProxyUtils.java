package com.winbaoxian.crawler.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public enum ProxyUtils {

    INSTANCE;

    private final static char CHAR_COLON = ':';

    private String[] ajaxProxy = null;

    public String getSeleniumProxy() {
        return getProxyIpPortString();
    }

    public String[] getAjaxProxyIpPort() {
        if (ArrayUtils.isEmpty(ajaxProxy)) {
            changeAjaxProxy();
        }
        return ajaxProxy;
    }

    public void changeAjaxProxy() {
        String ipPortStr = getProxyIpPortString();
        if (StringUtils.isBlank(ipPortStr)) {
            throw new RuntimeException("获取代理地址出错");
        }
        ajaxProxy = StringUtils.split(ipPortStr, CHAR_COLON);
    }

    //TODO 需要时实现该方法，使用代理精灵获取代理地址
    private String getProxyIpPortString() {
        return StringUtils.EMPTY;
    }

}
