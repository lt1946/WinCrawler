package com.winbaoxian.crawler.core.crawlertask.executor.ajax;

import com.alibaba.fastjson.JSONArray;
import com.winbaoxian.crawler.core.crawlertask.model.ajax.ExtractDataHttp;
import com.winbaoxian.crawler.model.core.TaskContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ExtractDataHttpStepExecutor extends AbstractAjaxStepExecutor<ExtractDataHttp> {

    @Override
    protected Object doExecute(ExtractDataHttp step, TaskContext context, Object tplResult) throws Exception {
        if (tplResult instanceof JSONArray) {
            context.getCurrentStoreResult().addAll((JSONArray) tplResult);
        } else {
            context.getCurrentStoreResult().add(tplResult);
        }
        return tplResult;
    }

}
