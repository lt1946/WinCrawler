package com.winbaoxian.crawler.core.crawlertask.executor.logic;

import com.winbaoxian.crawler.core.crawlertask.executor.MainStepsExecutor;
import com.winbaoxian.crawler.core.crawlertask.model.logic.AbstractGroupStep;
import com.winbaoxian.crawler.model.core.TaskContext;
import com.winbaoxian.crawler.model.dto.CrawlerResultDTO;
import com.winbaoxian.crawler.service.WinCrawlerService;
import com.winbaoxian.crawler.utils.JsonUtils;
import com.winbaoxian.crawler.utils.WinCrawlerLogUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;

@Slf4j
public abstract class AbstractGroupStepExecutor<T extends AbstractGroupStep> extends AbstractLogicStepExecutor<T> {

    @Resource
    protected MainStepsExecutor mainStepsExecutor;
    @Resource
    private WinCrawlerService winCrawlerService;

    @Override
    public void doExecute(T step, TaskContext context) throws Exception {
        String stepLogPrefix = WinCrawlerLogUtils.INSTANCE.getStepLogPrefix(step, context);
        log.info("[{}]{}", stepLogPrefix, StringUtils.repeat("<", (40 / (context.getStepDepth() + 1))));
        if (CollectionUtils.isEmpty(step.getStepList())) {
            return;
        }
        executeGroup(step, context);
        log.info("[{}]{}", stepLogPrefix, StringUtils.repeat(">", (40 / (context.getStepDepth() + 1))));
    }

    public abstract void executeGroup(T step, TaskContext context) throws Exception;

    protected boolean checkIfExists(T step, TaskContext context, String storeKey) {
        if (BooleanUtils.isNotTrue(step.getNeedStore()) || StringUtils.isBlank(storeKey)) {
            return false;
        }
        String stepLogPrefix = WinCrawlerLogUtils.INSTANCE.getStepLogPrefix(step, context);
        log.info("[{}]try to check result,taskId:{},key:{}", stepLogPrefix, context.getTaskDTO().getId(), storeKey);
        CrawlerResultDTO crawlerResult = winCrawlerService.getCrawlerResult(context.getTaskDTO().getId(), storeKey);
        if (crawlerResult != null && (StringUtils.isNotBlank(crawlerResult.getResult()) || StringUtils.isNotBlank(crawlerResult.getMsg()))) {
            log.info("[{}]exist result,taskId:{},key:{}", stepLogPrefix, context.getTaskDTO().getId(), storeKey);
            return true;
        } else {
            CrawlerResultDTO resultDTO = new CrawlerResultDTO();
            resultDTO.setTaskId(context.getTaskDTO().getId());
            resultDTO.setKeyName(storeKey);
            if (MapUtils.isNotEmpty(context.getCurrentStoreParams())) {
                resultDTO.setParams(JsonUtils.INSTANCE.toJSONString(context.getCurrentStoreParams()));
            }
            winCrawlerService.saveCrawlerResult(resultDTO);
            log.info("[{}]not exist result,taskId:{},key:{}", stepLogPrefix, context.getTaskDTO().getId(), storeKey);
            return false;
        }
    }

    protected void storeData(T step, TaskContext context, String storeKey) {
        if (BooleanUtils.isNotTrue(step.getNeedStore()) || StringUtils.isBlank(storeKey)) {
            return;
        }
        String stepLogPrefix = WinCrawlerLogUtils.INSTANCE.getStepLogPrefix(step, context);
        try {
            if (CollectionUtils.isEmpty(context.getCurrentStoreResult()) && StringUtils.isBlank(context.getCurrentStoreMsg())) {
                return;
            }
            log.info("[{}]try to store data,taskId:{},key:{},params:{}, result:{}, msg:{}", stepLogPrefix,
                    context.getTaskDTO().getId(), storeKey, JsonUtils.INSTANCE.toJSONString(context.getCurrentStoreParams()),
                    JsonUtils.INSTANCE.toJSONString(context.getCurrentStoreResult()), context.getCurrentStoreMsg());
            CrawlerResultDTO resultDTO = new CrawlerResultDTO();
            resultDTO.setTaskId(context.getTaskDTO().getId());
            resultDTO.setKeyName(storeKey);
            if (MapUtils.isNotEmpty(context.getCurrentStoreParams())) {
                resultDTO.setParams(JsonUtils.INSTANCE.toJSONString(context.getCurrentStoreParams()));
            }
            if (CollectionUtils.isNotEmpty(context.getCurrentStoreResult())) {
                resultDTO.setResult(JsonUtils.INSTANCE.toJSONString(context.getCurrentStoreResult()));
            }
            if (StringUtils.isNotBlank(context.getCurrentStoreMsg())) {
                resultDTO.setMsg(context.getCurrentStoreMsg());
            }
            winCrawlerService.saveCrawlerResult(resultDTO);
        } catch (Exception e) {
            log.error("[{}]storeData error, key:{}", stepLogPrefix, storeKey, e);
        } finally {
            context.clearCurrentData();
        }
    }


}
