package com.winbaoxian.crawler.core.crawlertask.executor.ajax;

import com.winbaoxian.crawler.core.crawlertask.model.ajax.Http;
import com.winbaoxian.crawler.model.core.TaskContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HttpStepExecutor extends AbstractAjaxStepExecutor<Http> {

    @Override
    protected Object doExecute(Http step, TaskContext context, Object tplResult) throws Exception {
        return tplResult;
    }

}
