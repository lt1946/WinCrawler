package com.winbaoxian.crawler.core.common;

import com.alibaba.fastjson.JSONObject;
import com.winbaoxian.common.freemarker.functions.*;
import com.winbaoxian.crawler.constant.WinCrawlerConstant;
import com.winbaoxian.crawler.enums.StepMode;
import com.winbaoxian.crawler.enums.StepType;
import com.winbaoxian.crawler.exception.WinCrawlerException;
import com.winbaoxian.crawler.model.core.TaskContext;
import com.winbaoxian.crawler.utils.JsonUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dongxuanliang252
 * @date 2019-03-06 14:31
 */
@Component
@Slf4j
public class ParamsExecutor {

    @Resource
    private FreeMarkerConfigurer freemarkerConfig;

    @PostConstruct
    public void initFunction() {
        try {
            Configuration configuration = freemarkerConfig.getConfiguration();
            configuration.setSetting(Configuration.NUMBER_FORMAT_KEY, "0.######");
            configuration.setSharedVariable("random", new RandomFunction());
            configuration.setSharedVariable("range", new RangeFunction());
            configuration.setSharedVariable("toJSONString", new ToJSONStringFunction());
            configuration.setSharedVariable("toJSON", new ToJSONFunction());
            configuration.setSharedVariable("toCookieString", new ToCookieStringFunction());
            configuration.setSharedVariable("descartes", new DescartesFunction());
            configuration.setSharedVariable("repeat", new RepeatFunction());
            configuration.setSharedVariable("matchNumber", new MatchNumberFunction());
            configuration.setSharedVariable("extractTagValues", new ExtractTagValuesFunction());
            configuration.setSharedVariable("getBirthday", new GetBirthdayFunction());
            configuration.setSharedVariable("escapeString", new EscapeStringFunction());
            configuration.setSharedVariable("jsoup", new JsoupFunction());
            configuration.setSharedVariable("jsoupXpath", new JsoupXpathFunction());
            configuration.setSharedVariable("aes", new AESFunction());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String render(String content, TaskContext context) {
        Map<String, Object> model = new HashMap<>();
        if (context != null) {
            if (MapUtils.isNotEmpty(context.getGlobalParams())) {
                model.putAll(context.getGlobalParams());
            }
            if (MapUtils.isNotEmpty(context.getCurrentStoreParams())) {
                model.putAll(context.getCurrentStoreParams());
            }
        }
        return render(content, model);
    }

    public String render(String content, Map<String, Object> model) {
        if (StringUtils.isBlank(content) || !needRender(content)) {
            return content;
        }
        try {
            log.info("Before render,{}", content);
            Template template = new Template(null, content, freemarkerConfig.getConfiguration());
            String ret = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
            log.info("After render,{}", ret);
            return ret;
        } catch (Exception e) {
            log.error("ParamsExecutor.render error, content:{}, model:{}", content, JsonUtils.INSTANCE.toJSONString(model));
            throw new WinCrawlerException(String.format("ParamsExecutor.render error, content:%s", content), e);
        }
    }

    private boolean needRender(String content) {
        if (content.contains(WinCrawlerConstant.FREEMARKER_VARIABLE_PREFIX) || content.contains(WinCrawlerConstant.FREEMARKER_CONDITION_PREFIX)) {
            return true;
        }
        return false;
    }


    /**
     * 以下情况不执行FTL
     * 当StepMode.ajax，并且key=tpl时;
     * 当value instanceof JSONArray (stepList)时;
     * 当WhileGroup，并且key=breakCondition时;
     * 当forGroup，并且key=storeKey时;
     * 当ifSignal,并且key=signalMsg时;
     *
     * @param jo
     * @param context
     */
    public void renderDeep(JSONObject jo, TaskContext context) {
        if (jo == null) {
            return;
        }
        String type = jo.getString("type");
        for (String key : jo.keySet()) {
            if ((StepType.extractDataHttp.getMode().equals(StepMode.ajax) && key.equals("tpl"))
                    || (StepType.whileGroup.name().equalsIgnoreCase(type) && key.equals("breakCondition"))
                    || (StepType.forGroup.name().equalsIgnoreCase(type) && key.equals("storeKey"))
                    || (StepType.ifSignal.name().equalsIgnoreCase(type) && key.equals("signalMsg"))) {
                continue;
            }
            Object value = jo.get(key);
            if (value instanceof String) {
                jo.put(key, render((String) value, context));
            } else if (value instanceof JSONObject) {
                JSONObject innerJo = (JSONObject) value;
                renderDeep(innerJo, context);
            }
        }
    }

}
