package com.winbaoxian.crawler.core.crawlertask.model.ajax;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExtractDataHttp extends AbstractAjaxStep {

    private static String DEFAULT_STEP_NAME = "提取数据请求";

    public ExtractDataHttp() {
        setName(DEFAULT_STEP_NAME);
    }

}
