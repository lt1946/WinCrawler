package com.winbaoxian.crawler.example;

import com.winbaoxian.common.freemarker.functions.*;
import com.winbaoxian.crawler.utils.JsonUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
public class ToolsTest {

    @Test
    public void ftl2String() {
        String content = readFile("src/test/java/Test.ftl");
        System.out.println(StringEscapeUtils.escapeJava(content));
    }

    @Test
    public void string2Ftl() {
        String content = readFile("src/test/java/Test.txt");
        System.out.println(StringEscapeUtils.unescapeJava(content));
    }


    @Test
    public void testRender() throws IOException, TemplateException {
        FreeMarkerConfig freeMarkerConfig = new FreeMarkerConfigurer();
        Configuration configuration = freeMarkerConfig.getConfiguration();
        initFreeMarker(configuration);
        String content = readFile("src/test/java/Test.ftl");
        String model = readFile("src/test/java/Model.json");
        Template template = new Template(null, content, configuration);
        String ret = FreeMarkerTemplateUtils.processTemplateIntoString(template, JsonUtils.INSTANCE.parseObject(model));
        System.out.println(ret);
    }

    private void initFreeMarker(Configuration configuration) throws TemplateException {
        configuration.setSetting(Configuration.NUMBER_FORMAT_KEY, "0.######");
        configuration.setSharedVariable("random", new RandomFunction());
        configuration.setSharedVariable("range", new RangeFunction());
        configuration.setSharedVariable("toJSONString", new ToJSONStringFunction());
        configuration.setSharedVariable("toJSON", new ToJSONFunction());
        configuration.setSharedVariable("toCookieString", new ToCookieStringFunction());
        configuration.setSharedVariable("descartes", new DescartesFunction());
        configuration.setSharedVariable("repeat", new RepeatFunction());
        configuration.setSharedVariable("matchNumber", new MatchNumberFunction());
        configuration.setSharedVariable("extractTagValues", new ExtractTagValuesFunction());
        configuration.setSharedVariable("getBirthday", new GetBirthdayFunction());
        configuration.setSharedVariable("escapeString", new EscapeStringFunction());
        configuration.setSharedVariable("jsoup", new JsoupFunction());
        configuration.setSharedVariable("jsoupXpath", new JsoupXpathFunction());
    }


    private String readFile(String fileName) {
        BufferedReader sbr = null;
        try {
            sbr = new BufferedReader(new FileReader(fileName));
            Stream<String> lineStreams = sbr.lines();
            List<String> lines = lineStreams.collect(Collectors.toList());
            return StringUtils.join(lines, "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (sbr != null) {
                try {
                    sbr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
